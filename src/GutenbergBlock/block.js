( function( editor, components, i18n, element ) {
	var el = element.createElement,
		registerBlockType = wp.blocks.registerBlockType,
		InspectorControls = wp.editor.InspectorControls,
		SelectControl = wp.components.SelectControl,
		allCtas = [],
		ctaSelectOptions = [{label:'Select From Below',value:0}];

	$.ajax({
		method: 'GET',
		url: ctaBlockInfo.restUrl,
		dataType: 'json',
	}).then(function(r){
		for(var key in r ){
			var cta = r[key];
			allCtas.push(cta);
			ctaSelectOptions.push({label:cta.title.rendered,value:cta.id});
		}
	});
	var getSelectedCta = function(id){
		id = parseInt(id);
		for(var key in allCtas ){
			if(allCtas[key].id === id){
				return allCtas[key];
			}
		}
	};
	registerBlockType( 'hipcta-block/hip-cta', {
		title: i18n.__( 'HipCTA' ),
		description: i18n.__( 'Block for displaying a CTA' ),
		icon:'share-alt2',
		category: 'common',
		attributes: {
			ctaId: {
				type: 'string',
				default: '0'
			},
			imageLink: {
				type: 'string',
				default: ctaBlockInfo.assetsUrl+'/img/cta-placeholder.jpg'
			},
			ctaLink : {
				type: 'string',
				default: '#'
			}

		},

		edit: function( props ) {

			var attributes = props.attributes,
				ctaId = attributes.ctaId;

			return [
				el( InspectorControls, { key: 'inspector' },
					el( components.PanelBody, {
							title: i18n.__( 'HipCTA Options' ),
							className: 'block-options',
							initialOpen: true,
						},
						el( SelectControl,{
							type: 'string',
							label: i18n.__( 'Select CTA' ),
							value: ctaId,
							options: ctaSelectOptions,
							onChange: function( newctaId ) {
								var selectedCta =  getSelectedCta(newctaId);
								props.setAttributes({ ctaId: newctaId });
								if(newctaId === '0'){
									props.setAttributes({ctaLink :'#'});
									props.setAttributes({imageLink:ctaBlockInfo.assetsUrl+'/img/cta-placeholder.jpg'})
								}else{
									props.setAttributes({ctaLink : selectedCta.cta_meta.link_url.length > 0 ? selectedCta.cta_meta.link_url : '#'});
									props.setAttributes({imageLink:selectedCta.cta_meta.image_url})
								}


							},
						})
					),
				),
				el( 'div', {key:'hipCta-container', className: props.className },
					el('a',{
						href: attributes.ctaLink,
						className: 'hipcta_image_cta',
					},el('img',{
							src: attributes.imageLink
						})
					)
				)
			];
		},

		save: function( props ) {
			var attributes = props.attributes;
			return (
				el( 'div', {key:'hipCta-container', className: 'hipCta-block' },
					el('a',{
							href: attributes.ctaLink,
							className: 'hipcta_image_cta',
						},el('img',{
							src: attributes.imageLink
						})
					)
				)
			);
		},
	});

})(
	window.wp.editor,
	window.wp.components,
	window.wp.i18n,
	window.wp.element,
);
