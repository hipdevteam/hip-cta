<?php

namespace HipCTA\GutenbergBlock;

class Block
{
	/**
	 * @var array
	 */
	protected $container;
	/**
	 * method __construct
	 * @param array
	 */
	public function __construct($container)
	{
		$this->container = $container;
		add_action('init', array($this,'initBlock'));
	}

	/**
	 * init gutenberg HipCTA block
	 * @return void
	 */

	public function initBlock()
	{
		wp_register_script('hipcta-block-script', plugins_url('block.js', __FILE__), array( 'jquery','wp-blocks', 'wp-element', 'wp-i18n' ), $this->container['version'], true);
		// Styles.
		wp_register_style('hipcta-block-editor-style', plugins_url('editor.css', __FILE__), array( 'wp-edit-blocks' ));

		wp_register_style('hipcta-block-frontend-style', plugins_url('style.css', __FILE__), array( 'wp-edit-blocks' ));

		register_block_type('hipcta-block/hip-cta', array(
			'editor_script' => 'hipcta-block-script',
			'editor_style' => 'hipcta-block-editor-style',
			'style' => 'hipcta-block-frontend-style',
		));

		wp_localize_script('hipcta-block-script', 'ctaBlockInfo', array(
			'restUrl' => rest_url('wp/v2/hipcta'),
			'assetsUrl' => $this->container['plugin_url'].'/assets'
		));
		add_action('rest_api_init', [$this, 'addMetasInRestApi']);
	}

	/**
	 * add meta fields in rest api
	 * @return void
	 */

	public function addMetasInRestApi()
	{

		register_rest_field($this->container['post_type'], 'cta_meta', array(
			'get_callback' => array($this, 'getMetaForRestApi'),
			'schema' => null,
		));
	}
	/**
	 * get meta fields for rest api
	 * @param object
	 * @return array
	 */

	public function getMetaForRestApi($object)
	{
		$metas = get_post_meta($object['id'], 'hipcta', true);

		return array(
			'image_url' => wp_get_attachment_image_src($metas['image'],'large')[0],
			'link_url'  => $metas['link_url']
		);
	}
}
